package net.codejava.springmvc;


import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.List;

import com.codejava.models.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mysql.jdbc.Statement;
import com.sun.javafx.collections.MappingChange.Map;
//import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;






import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model,HttpServletRequest request) {
		
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		HttpSession session=request.getSession();
		String user=(String) session.getAttribute("user");
		if(user==null)
		{
			user="";
		}
		model.addAttribute("sess", user);
		
		return "home";
	}
	
	@RequestMapping(value = "/loginPage", method = RequestMethod.GET)
	public String loginPage()
	{
		return "login";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request)
	{
		HttpSession session=request.getSession();
		//session.setAttribute("user", "");
		session.invalidate();
		return "home";
	}
	
	
	@RequestMapping(value = "/rate", method = RequestMethod.GET)
	public String rate(HttpServletRequest request,Model model) throws Exception
	{
		String a=request.getParameter("id");
		String e=request.getParameter("email");
		int n=Integer.parseInt(a);
		int rate=n/10;
		int c=n%10;
		DoctorDao obj=new DoctorDao();
		CommentDto li=obj.GetCommentByID(c,e);
		float f=li.rate;
		f=(float) ((f+rate)/2.0);
		int st=obj.updateComment(f, e, c);
		
		model.addAttribute("id", c);
		return "rate";
	}
	
	
	
	
@RequestMapping(value = "/login", method = RequestMethod.POST)
	
	public String login(HttpServletRequest request,HttpServletResponse res) throws Exception
	{
	String a=request.getParameter("user");
	String p=request.getParameter("pass");
	HttpSession session=request.getSession();
	session.setAttribute("user", a);
	
	return "home";
	}
		
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test(HttpServletRequest request,Model model) throws Exception
	{
		
		String a=request.getParameter("id");
		String id="";
		String name="";
		String url1="";
		Connection conn = null;
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");
		      //STEP 3: Open a connection (by providing DB URL,User & Password)
		      // Here "test" is database name
		      System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","");
			  System.out.println("Connected...");
			  
			  Statement stmt = null;
			  stmt = (Statement) conn.createStatement();
				String sql;
				sql = "SELECT id,name,url FROM members where id="+a;
				ResultSet rs = stmt.executeQuery(sql);

				while(rs.next()){
					id = rs.getString("id");
					name = rs.getString("name");
					url1 = rs.getString("url");
				}
				
				rs.close();
			      stmt.close();
			      conn.close();

				
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }
		   HttpSession session=request.getSession();
			String user=(String) session.getAttribute("user");
			model.addAttribute("sess", user);
		model.addAttribute("id", id);
		model.addAttribute("na", name);
		model.addAttribute("ur", url1);
		DoctorDao obj=new DoctorDao();
		List<CommentDto> li=obj.GetAllComments(id);
		
		model.addAttribute("obj", li);
		
		return "detail";
		}
	
	
	
	
	
	
	
	@RequestMapping(value = "/comment", method = RequestMethod.POST)
	
	public String comment(HttpServletRequest request,Model model) throws Exception
	{
		HttpSession session=request.getSession();
		if(session==null)
		{
			return "error";
		}
		else
		{
			String user=(String) session.getAttribute("user");
			if(user==null)
			{
				return "error";
			}
			else
			{
				String id=request.getParameter("id1");
				String a=request.getParameter("name");
				String e=request.getParameter("email");
				String m=request.getParameter("message");
				DoctorDao obj=new DoctorDao();
				int r=obj.SaveComment(id, a, e, m);
				
				
				model.addAttribute("id", id);
				 return "success";
			}
		}
			
		}
		
		
	
	
	
	
	
}
