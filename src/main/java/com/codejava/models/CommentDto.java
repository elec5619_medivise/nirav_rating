package com.codejava.models;

public class CommentDto {
	public String name;
	public String email;
	public String message;
	public float rate;
	public CommentDto() {
		name="";
		email="";
		message="";
		rate=0;
		// TODO Auto-generated constructor stub
	}
	public String getName()
	{
		return name;
	}
	public String getEmail()
	{
		return email;
	}
	public String getMessage()
	{
		return message;
	}
	public float getRate()
	{
		return rate;
	}
	

}
