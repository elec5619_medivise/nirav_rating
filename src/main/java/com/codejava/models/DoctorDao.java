package com.codejava.models;


import java.sql.*;
import java.util.*;

public class DoctorDao
{

	private DBhandler dbObj = null;

	public DoctorDao()  throws Exception
	{
		dbObj = new DBhandler();
	}

	public int SaveComment(String id,String n,String e,String m) throws Exception
	{
		try
		{
			if(dbObj.OpenConnection() == true)
			{
				String query = "";

				
					query = "Insert Into comment(pid,name,email,message,rate) Values('" +id+"','" +n+"','" +e+"','" +m+"','" +0+"')";
					System.out.println("inser query is:" + query);
					return dbObj.InsertQuery(query);
				
			}
		}catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			dbObj.CloseConnection();
		}

		return 0;

	}
	
	
	
	
	public int updateComment(float rate,String em,int id) throws Exception
	{
		try
		{
			if(dbObj.OpenConnection() == true)
			{
				String query = "";

				
				query = "Update comment Set rate=" +rate+ " Where pid=" + id+" AND email like '"+em+"'";

					
					System.out.println("inser query is:" + query);
					return dbObj.UpdateQuery(query);

				
			}
		}catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			dbObj.CloseConnection();
		}

		return 0;

	}
	
	
		
	public List<CommentDto> GetAllComments(String id) throws Exception
	{
		List<CommentDto> stdList = null;

		try
		{
			if(dbObj.OpenConnection() == true)
			{
				stdList = new ArrayList<CommentDto>();
				String sqlQuery = "Select * from comment where pid="+id;
				ResultSet rs = dbObj.ExecuteQuery(sqlQuery);
				while(rs.next())
				{
					CommentDto dto = FillDTO(rs);
					stdList.add(dto);
				}
			}
		}catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			dbObj.CloseConnection();
		}
		return stdList;
	}//End of GetAllStudents()
	public CommentDto GetCommentByID(int Id,String e) throws SQLException
		{
		CommentDto std = null;

			try
			{
				if(dbObj.OpenConnection() == true)
				{
					std = new CommentDto();
					String sqlQuery = "Select * from comment Where pid=" + Id +" AND email='"+e+"'";
					ResultSet rs = dbObj.ExecuteQuery(sqlQuery);
					if(rs.next())
					{
						std = FillDTO(rs);
					}
				}
			}catch(Exception ex)
			{
				return null;
			}
			finally
			{
				dbObj.CloseConnection();
			}
			return std;
	}//End of GetAllStudents()

	private CommentDto FillDTO(ResultSet rs) throws SQLException
	{
		CommentDto dto = new CommentDto();
		
		dto.name = rs.getString("name");
		dto.email = rs.getString("email");
		dto.message	= rs.getString("message");
		dto.rate	= rs.getFloat("rate");

		return dto;
	}

}
