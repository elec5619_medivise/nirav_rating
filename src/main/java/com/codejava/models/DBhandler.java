package com.codejava.models;
import java.sql.*;
import java.util.*;

public class DBhandler
{
	String driverName = "com.mysql.jdbc.Driver";
	String dbURL = "jdbc:mysql://localhost:3306/test";  //  Connect the server and the database
	String userName="root";
	String userPwd="";
	Connection con;

	public Boolean OpenConnection()throws Exception{
		try
		{
			//Loading Driver
			System.out.println("Loading Driver");
			Class.forName(driverName);
			//Opening Connection
			System.out.println("Opening Connection");
			con = DriverManager.getConnection(dbURL,userName,userPwd);
			System.out.println("Connection Opened Successfull!");
			return true;
		}
	  	catch (Exception e)
	  	{
  			throw e;
  			//return false;
		}
	}
	public Boolean CloseConnection()
	{
		try
		{
			if(con != null)
				con.close();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	public ResultSet ExecuteQuery(String sqlQuery)throws Exception
	{
		try
		{
			//Create Statement
			System.out.println("Creating statement");
			Statement st = con.createStatement();

			//Execute Query
			System.out.println("Executing Query");
			ResultSet rs = st.executeQuery(sqlQuery);

			return rs;
		}
		catch (Exception e)
		{
			throw e;
			//return null;
		}
	}
	public int UpdateQuery(String sqlQuery) throws Exception
	{
			//Create Statement
			System.out.println("Creating statement");
			Statement stmt = con.createStatement();

			//Execute Query
			System.out.println("Executing Query");
			int id = stmt.executeUpdate(sqlQuery);
			return id;
	}

	public int InsertQuery(String sqlQuery) throws Exception
	{
		//Create Statement
		System.out.println("Creating statement");
		Statement stmt = con.createStatement();

		//Execute Query
		System.out.println("Executing Query");
		stmt.executeUpdate(sqlQuery,Statement.RETURN_GENERATED_KEYS);
		ResultSet rs = stmt.getGeneratedKeys();
		if ( rs.next() ) {
		    // Retrieve the auto generated key(s).
		    int key = rs.getInt(1);	 
		    return key;
		}//End of

		return -1;
	}
	
} //End of DBHandler